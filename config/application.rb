require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Orangereya
  class Application < Rails::Application
    I18n.config.enforce_available_locales = true
    config.i18n.default_locale = :ru
    config.autoload_paths += %W(#{config.root}/app/models/ckeditor)
    config.generators do |q|
      q.test_framework :rspec
    end
  end
end
