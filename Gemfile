source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.0'
# Use sqlite3 as the database for Active Record
gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.3'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer',  platforms: :ruby
gem 'slim', github: 'slim-template/slim'
gem 'devise'
gem 'simple_form'
gem 'unicode'
gem 'kaminari'
gem 'bootstrap-sass'
gem 'bootstrap-kaminari-views'
gem 'autoprefixer-rails'
gem 'nokogiri'
gem 'dragonfly'

gem 'carrierwave'
gem 'mini_magick'

gem 'puma'

gem 'ckeditor'

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'compass-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0',          group: :doc

# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
gem 'spring', group: :development

group :devlopment, :test do
  gem 'timecop'

  # deploy
  gem 'capistrano'
  gem 'rvm-capistrano'

  # tests
  gem 'rspec'
  gem 'rspec-rails', '~> 3.0.0'
  gem 'webmock'
  gem 'factory_girl_rails'
  gem 'shoulda-matchers', require: false

  # docs

  gem 'yard'
  gem 'yard-activerecord'
  gem 'guard-yard'

  # guard stuff
  #
  # command line tool to easily handle events on file system modifications
  gem 'guard'
  #
  # FSEvents API with signals handled (without RubyCocoa)
  gem 'rb-fsevent', require: false
  #
  # Mac OS X User Notifications for Guard
  gem 'terminal-notifier-guard', require: false
  #
  # allows to automatically & intelligently launch specs when files are modified
  gem 'guard-rspec'
end

group :development do
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'pry-rails'
  gem 'simplecov'
  gem 'brakeman'
  gem 'rails_best_practices'
  gem 'rubocop', require: false
  gem 'rubocop-rspec', require: false
end

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'
gem 'rails_12factor', group: :production
# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]
