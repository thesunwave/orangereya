class CreateOrderAudios < ActiveRecord::Migration
  def change
    create_table :order_audios do |t|
      t.string :subject
      t.string :email
      t.text :text
      t.boolean :success, default: false, null: false

      t.timestamps null: false
    end
  end
end
