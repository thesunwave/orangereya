class Admin::OrderAudioController < ApplicationController

  layout 'admin'
  before_action :authenticate_user!
  before_action :find_order, only: %w(success restore)

  def index
    @orders = OrderAudio.all.order('created_at DESC').where(success: false)
    @success = OrderAudio.all.order('created_at DESC').where(success: true)
  end

  def show
    @order = OrderAudio.find(params[:id])
  end

  def destroy
    @order = OrderAudio.find(params[:id])
    @order.destroy

    redirect_to '/admin/order_audio'
  end
=begin
  def update
    @order = OrderAudio.find(params[:id])

    if @order.update(order_params)
      redirect_to '/admin/order_audio'
    else
      render 'show'
    end
  end
=end
  def success
    @order.update_attribute :success, true
    redirect_to '/admin/order_audio'
  end

  def restore
    @order.update_attribute :success, false
    redirect_to '/admin/order_audio'
  end

private

  def order_params
    params.require(:order_audio).permit(:success)
  end

  def find_order
    @order = OrderAudio.find params[:id]
  end

end
