class Admin::PostsController < ApplicationController
  layout 'admin'
  before_action :authenticate_user!

  def index
    @posts = Post.all.order('created_at DESC')
  end

  def new
    @post = Post.new
  end

  def edit
    @post = Post.find(params[:id])
  end

  def create
    @post = Post.create(post_params)
    redirect_to '/admin/posts'
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy

    redirect_to '/admin/posts'
  end

  def update
    @post = Post.find(params[:id])

    if @post.update(post_params)
      redirect_to '/admin/posts'
    else
      render 'edit'
    end
  end

private

  def post_params
    params.require(:post).permit(:title, :description, :text)
  end
end
