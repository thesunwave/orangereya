class OrderAudioController < ApplicationController
  def new
    @order = OrderAudio.new
  end

  def create
    @order = OrderAudio.create(order_params)
      if @order.save
        redirect_to root_path
        flash[:notice] = "Order was been save"
      else
        render 'new'
      end
  end

private

  def order_params
    params.require(:order_audio).permit(:subject, :email, :text)
  end

end
