module ApplicationHelper
  def title_page
    title = content_for?(:title) ? content_for(:title) : nil
    title ? t('views.title.title', title: title.chomp) : t('views.title.title')
  end
end
