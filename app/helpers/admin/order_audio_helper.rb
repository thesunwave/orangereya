module Admin::OrderAudioHelper
  def set_success_btn(order)
    if order.success?
      link_to 'восстановить',
              restore_admin_order_audio_path(order),
              class: 'btn btn-success btn-mini',
              method: :post
    else
      link_to 'Выполнено',
              success_admin_order_audio_path(order),
              class: 'btn btn-warning btn-mini',
              method: :post
    end
  end

  def destroy_btn(order)
    if order.success?
      link_to 'Удалить',
              admin_order_audio_path(order),
              class: 'btn btn-success btn-mini',
              method: :delete,
              confirm: "Вы уверены?"
            end
  end
end