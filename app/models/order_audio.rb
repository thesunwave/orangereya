class OrderAudio < ActiveRecord::Base
  validates :subject, presence: true
  validates :email, presence: true, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }
  validates :text, presence: true, length: { minimum: 20 }



end
